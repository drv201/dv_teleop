# Copyright 2016 Open Source Robotics Foundation, Inc.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import rclpy
from rclpy.node import Node
from rclpy.qos import qos_profile_system_default

from std_msgs.msg import String
from geometry_msgs.msg import Twist


class MinimalTeleop(Node):

    def __init__(self):
        super().__init__('minimal_teleop')
        self.publisher_ = self.create_publisher(Twist, '/cmd_vel',
                                           qos_profile_system_default)
        timer_period = 0.5  # seconds
        self.timer = self.create_timer(timer_period, self.timer_callback)
        self.i = 0
        print("Running talker")

    def timer_callback(self):
        twist = Twist()
        twist.linear.x = 0.5; twist.linear.y = 0.0; twist.linear.z = 0.0
        twist.angular.x = 0.0; twist.angular.y = 0.0; twist.angular.z = 1.0
        self.publisher_.publish(twist)



def main(args=None):
    rclpy.init(args=args)

    minimal_teleop = MinimalTeleop()

    rclpy.spin(minimal_teleop)

    # Destroy the node explicitly
    # (optional - otherwise it will be done automatically
    # when the garbage collector destroys the node object)
    minimal_publisher.destroy_node()
    rclpy.shutdown()


if __name__ == '__main__':
    main()
