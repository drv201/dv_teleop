# Copyright 2016 Open Source Robotics Foundation, Inc.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

from dataclasses import dataclass
from geometry_msgs.msg import PoseStamped, Quaternion, Twist

import math
from math import atan, atan2, cos, sin, fabs
import message_filters
from nav_msgs.msg import Odometry

import rclpy
from rclpy.node import Node
from rclpy.qos import qos_profile_system_default

from sensor_msgs.msg import LaserScan, Imu
from std_msgs.msg import String

from tf_transformations import euler_from_quaternion

@dataclass
class Point:
    x: float
    y: float
    z: float

@dataclass
class Quaternion:
    x: float
    y: float
    z: float
    w: float

@dataclass
class Xy:
    x: float
    y: float

class Controller(Node):

    def __init__(self):
        super().__init__('control_teleop')

        print("Running controller") # Just so we know our controller is running

        self.subscription = self.create_subscription(
            LaserScan,
            '/scan',
            self.listener_callback,
            10
        )

        self.pose = self.create_subscription(
            Odometry,
            '/odom',
            self.pose_callback,
            10
        )

        self.position = Point(x=0.0, y=0.0, z=0.0)
        self.heading = 0.0  # Holds heading from -pi/2 to pi/2 Overlap can be a pain
        
        self.plan = [( 1.0,  1.0),      # Arbitrary list of target way-points
                     (-1.0,  1.0), 
                     (-1.0, -1.0), 
                     ( 1.0, -1.0)]
        self.plan_step = 0
        
        t = self.plan[self.plan_step]
        self.target = Xy(x = t[0],      # Get first target to use on startup
                         y = t[1])

        # self.subscription  # prevent unused variable warning
        self.publisher_ = self.create_publisher(Twist, '/cmd_vel',
                                    qos_profile_system_default)

# Update the current target to the next one on the list,
# and start again if you run out of targets:
    def increment_target(self):
        self.plan_step += 1
        if self.plan_step >= len(self.plan):
            self.plan_step = 0
        
        t = self.plan[self.plan_step]
        print(f"\nNext target={t}")

        self.target = Xy(x = t[0], 
                         y = t[1])

# Convenience function to publish speed and turn rate:
    def publish_twist(self, speed, turn_rate):
        twist = Twist()
        twist.linear.x = speed # m/sec
        twist.angular.z = turn_rate # radians/sec
        twist.linear.y = 0.0; twist.linear.z = 0.0
        twist.angular.x = 0.0; twist.angular.y = 0.0; 
        self.publisher_.publish(twist)

# Called each time a pose comes in. The msg contains 
# position - with x, y, z componets, and
# orientation - with a pose quarternion. That's 
# converted here into roll, pitch yaw, and yaw is 
# saved in self.heading. Goes from -pi to pi
    def pose_callback(self, msg):
        self.position =  msg.pose.pose.position
        
        orientation_list = [msg.pose.pose.orientation.x,
                            msg.pose.pose.orientation.y,
                            msg.pose.pose.orientation.z,
                            msg.pose.pose.orientation.w] 
        (roll, pitch, yaw) = euler_from_quaternion(orientation_list)    
        self.heading = yaw 
        # print(f"r: {roll*57.2957795:.1f}, p: {pitch*57.2957795:.1f}, y: {yaw*57.2957795:.1f}")

    def control(self):
        x = self.target.x - self.position.x
        y = self.target.y - self.position.y
        azimuth = atan2(y,x)
        range = math.sqrt(y**2+x**2)

        if range < 0.05:
            print(" * ", end='', flush=True)
            self.increment_target()
            return()

        error = azimuth-self.heading
        if fabs(error) > 0.1:
            self.publish_twist(0.0, error/1.5)
            print("t", end='', flush = True)
        else:
            self.publish_twist(0.1, 0.0)
            print("m", end='', flush=True)



# Callback that receives laser scans in its msg
# Also used here as the master clock for control
# See: docs.ros.org/en/melodic/api/sensor_msgs/html/msg/LaserScan.html
# or Google. That's for melodic, but humble seems to use the same
# message format

# I'm calling out to a seperate control function to make
# the purpose clear

    def listener_callback(self, msg):
        # print("Distance ahead:", msg.ranges[0])
        self.control()

def main(args=None):
    rclpy.init(args=args)

    control_teleop = Controller()
    
    rclpy.spin(control_teleop)
    scan_subscriber.destroy_node()
    rclpy.shutdown()


if __name__ == '__main__':
    main()
