# Copyright 2016 Open Source Robotics Foundation, Inc.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.


from dataclasses import dataclass
from geometry_msgs.msg import PoseStamped
from geometry_msgs.msg import Quaternion
from geometry_msgs.msg import Twist

import math
from math import atan, atan2, cos, sin
import message_filters
from nav_msgs.msg import Odometry


import rclpy
from rclpy.node import Node
from rclpy.qos import qos_profile_system_default

from sensor_msgs.msg import LaserScan
from sensor_msgs.msg import Imu
from std_msgs.msg import String

from tf_transformations import euler_from_quaternion


@dataclass
class Point:
    x: float
    y: float
    z: float

@dataclass
class Quaternion:
    x: float
    y: float
    z: float
    w: float

@dataclass
class Xy:
    x: float
    y: float

class ScanSubscriber(Node):

    def __init__(self):
        super().__init__('scan_subscriber')
        self.subscription = self.create_subscription(
            LaserScan,
            '/scan',
            self.listener_callback,
            10
        )

        self.pose = self.create_subscription(
            Odometry,
            '/odom',
            self.pose_callback,
            10
        )
        self.position = Point(x=0.0, y=0.0, z=0.0)
        self.ftime = 1.0
        self.target = Xy(x=2.0, y=2.0)
        self.orientation = Quaternion(x=0.0, y=0.0, z=0.0, w=0.0)
        self.heading = 0.0
        self.target_heading = 0.0
        self.north_target=(2.0, 0.0)

        
        self.south_target=(0.0, 0.0)
        self.target = self.north_target
        
        self.plan = [( 1.0,  1.0), 
                     (-1.0,  1.0), 
                     (-1.0, -1.0), 
                     ( 1.0, -1.0)]
        self.plan_step = 0
        
        t = self.plan[self.plan_step]
        print("Running subscriber")
        print(f"t={t}, t.0={t[0]}")
        
        self.target = Xy(x = t[0], 
                         y = t[1])
        

# Add a subscriber here to /odom to find out where the robot has been
# To really spice things up, also use IMU
# Alternatively, use some SLAM code to give your absolute position

        self.subscription  # prevent unused variable warning
        self.publisher_ = self.create_publisher(Twist, '/cmd_vel',
                                    qos_profile_system_default)

    def command_twist(self, x, z):
        twist = Twist()
        twist.linear.x = x
        twist.angular.z = z 
        twist.linear.y = 0.0; twist.linear.z = 0.0
        twist.angular.x = 0.0; twist.angular.y = 0.0; 
        self.publisher_.publish(twist)

    def pose_callback(self, msg):
        position = msg.pose.pose.position
        self.position = position
        print(self.position)
        self.orientation = msg.pose.pose.orientation
        orientation_list = [msg.pose.pose.orientation.x,
                            msg.pose.pose.orientation.y,
                            msg.pose.pose.orientation.z,
                            msg.pose.pose.orientation.w] 
        (roll, pitch, yaw) = euler_from_quaternion(orientation_list)    
        self.heading = yaw + math.pi

        # print(f"r: {roll*57.2957795:.1f}, p: {pitch*57.2957795:.1f}, y: {yaw*57.2957795:.1f}")

    def steer(self):

        print(f"My   x: {self.position.x:.1f}, y: {self.position.y:.1f}\n"
              f"Target: {target.x:.1f}, y: {target.y:.1f}")

        target_heading = atan2((target.x - self.position.x), 
                               (target.y - self.position.y))

        error = (target_heading - self.heading)/2.0
        print(f"Heading: {self.heading*180/3.1415926:.1f}, "
                f"Target: {target_heading*180/3.1415926:.1f}, "
                f"Error: {error*180/3.1415926:.1f}" )
        if math.fabs(error) < 0.01:
            return True
        else:
            print(f"error: {error:.2f}")
            self.command_twist(0.0, error)
            return False

    def drive(self):
        target = Xy(x = self.plan[self.plan_step][0], 
                    y = self.plan[self.plan_step][1])
        pos = self.position

        error = math.sqrt((target.y - self.position.y)**2 +  
                          (target.x - self.position.x)**2)
        print(f"Target: {target.x:.2}, {target.y:.2} "
              f"Actual: {self.position.x:.2}, {self.position.y:.2} "
              f" Error is {error:.2}")

        if error < 0.05:
            self.plan_step = (self.plan_step + 1) % 4
            print(f"\t\t\t************ Plan step is now: {self.plan_step}")
            return True
        else:
            self.command_twist(0.2, 0.0)
            return False


    def listener_callback(self, msg):
        target = Xy(x = self.plan[self.plan_step][0], 
                    y = self.plan[self.plan_step][1])
    
        print(f"My   x: {self.position.x:.1f}, y: {self.position.y:.1f}")
        target_heading = atan2((self.target.y - self.position.y), 
                               (self.target.x - self.position.x))
        
        print(f"Target: {target_heading*180/3.1415926:.1f}")

        if abs(self.heading - target_heading) > 0.1: 
            self.command_twist(0.0, 0.1)
        else:
            self.command_twist(0.2, 0.0)
        




        # if self.state == 0:
        #     if self.steer():
        #         self.state = 1
        # elif self.state == 1:
        #     if self.drive():
        #         self.state = 0

           


def main(args=None):
    rclpy.init(args=args)

    scan_subscriber = ScanSubscriber()
    rclpy.spin(scan_subscriber)
    scan_subscriber.destroy_node()
    rclpy.shutdown()


if __name__ == '__main__':
    main()
